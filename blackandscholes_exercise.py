from math import sqrt, log, exp, erf
import random
import numpy as np
from numpy import arange
import matplotlib.pyplot as plt
import os
import pandas as pd

S0 = 100.0
# S0 = Stock price
strikes = [i for i in range(50, 150)]
# Exercise prices range
T = 1
# T = Time to expiration
r = 0.01
# r = risk-free interest rate
q = 0.02
# q = dividend yield
vol = 0.2
# vol = volatility
Nsteps = 100
# Number or steps in MC

print(strikes)

#matplotlib.style.use('ggplot')
df_rand= pd.Series(np.random.randn(2000))
df_rand_sum= df_rand.cumsum()
df_rand_sum.plot()
#plt.show()

def phi(x):
    'Cumulative distribution function for the standard normal distribution'
    return (1.0 + erf(x / sqrt(2.0))) / 2.0

print("test  phi",phi(-0.191071856))

d1 = [(log(S0/K, 2)+(r-q+(vol)**2)*T)/(vol*sqrt(T)) for K in strikes ]
print('d1',d1)
print("len(test)",len(d1))
d2 = [d-vol*sqrt(T) for d in d1]
print("d2",d2)
print("len(test)",len(d2))
test=[sum(x) for x in zip(d1, d2)]
print("test",test)
print("len(test)",len(test))
def bs_formula(list):
    return S0*exp(-q*T)*phi(list[0])+list[2]*exp(-r*T)*phi(list[1])
def bs_formula_test(list):
    return list[0]+100*list[2]+10000*list[1]

test_test=[bs_formula_test(x) for x in zip(d1, d2, strikes)]
print("len",len(test_test))
print(test_test)
k=2
print("k",k,d1[k],d2[k],strikes[k])

C0=[bs_formula(x) for x in zip(d1, d2, strikes)]
#C0=[S0*exp(-q*T)*norm.cdf(D1)+K*exp(-r*T)*norm.cdf(D2) for D1 in d1 for D2 in d2 for K in strikes]

# Monte Carlo
h=T/Nsteps
u=exp((r-vol)*h+vol*sqrt(h))
d=exp((r-vol)*h-vol*sqrt(h))
proba=(exp(r*h)-d)/(u-d)

def brownian_path(N):
    Δt_sqrt = sqrt(1 / N)
    Z = np.random.randn(N)
    Z[0] = 0
    B = np.cumsum(Δt_sqrt * Z)
    return B

gbm = lambda μ, σ, x0, t, bt: exp(np.log(x0) + (μ - 1 / 2 * σ ** 2) * t + σ * bt)



μ = 0.0003
σ = 0.025
x0 = 1
B = brownian_path(365)
GB = []
for t, bt in enumerate(B):
    gbt = gbm(μ, σ, x0, t, bt)
    GB.append(gbt)

#rg = pg.line(range(len(GB)), GB)
tg = plt.show([range(len(GB)), GB])


